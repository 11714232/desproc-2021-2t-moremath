﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScript : MonoBehaviour
{
    public Rigidbody2D rb;
    public bool isAnswer;
    FixedJoint2D joint;

    private void Awake()
    {
       
    }
    // Start is called before the first frame update
    void Start()
    {
        if(isAnswer)
        {
            gameObject.tag = "Answer";
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        rb.velocity = Vector3.zero; 
    }

    
}
