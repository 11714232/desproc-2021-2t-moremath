﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlay : MonoBehaviour
{
    public static GamePlay instance;
 
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void AdvanceFloor() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //usses index to load next scene
    }

    public void ResetFloor()
    {
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //Uses index to load current scene
    }
}
