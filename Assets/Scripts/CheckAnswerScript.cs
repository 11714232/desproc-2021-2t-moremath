﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAnswerScript : MonoBehaviour
{
    [SerializeField] private DoorScript door;

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Answer"))
        {
            door.OpenDoor();
            Debug.Log("Right Answer");
        }
        if(collision.CompareTag("Choices"))
        {
            GamePlay.instance.ResetFloor();
        }
    }
}
