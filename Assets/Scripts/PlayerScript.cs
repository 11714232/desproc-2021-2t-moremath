﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerScript : MonoBehaviour
{
    public float moveSpeed = 5.0f;
    public Rigidbody2D rb;
    //public Joystick joystick;
    public Animator animator;

    private Vector2 movement;
    Vector2 movementUpdate;

    // Start is called before the first frame update

    void Update()
    {
        ProcessInputs();
        Animate();
    }

    void FixedUpdate()
    {
        Move();
    }

    void ProcessInputs()
    {
        float moveX = SimpleInput.GetAxis("Horizontal");
        float moveY = SimpleInput.GetAxis("Vertical");

        if ((moveX == 0 && moveY == 0) && movement.x != 0 || movement.y != 0)
        {
            movementUpdate = movement;
        }

        movement = new Vector2(moveX, moveY).normalized;
    }

    void Move()
    {
        rb.velocity = new Vector2(movement.x * moveSpeed, movement.y * moveSpeed);
    }

    void Animate()
    {
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.magnitude);

        animator.SetFloat("HorizontalUpdate", movementUpdate.x);
        animator.SetFloat("VerticalUpdate", movementUpdate.y);
    }
}
